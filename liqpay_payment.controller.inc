<?php
class LiqpayPaymentMethodController extends PaymentMethodController {
  public $controller_data_defaults = array(
	'version' => 3,
	'public_key' => '',
	'private_key' => '',
	'description' => '',
	'type' => 'buy',
	'server_url' => 'liqpay_payment/server',
	'result_url' => 'liqpay_payment/result',
	'pay_way' => 'card,liqpay,delayed,invoice,privat24',
	'sandbox' => TRUE,
	'language' => 'ru',
  );
  
  public $payment_method_configuration_form_elements_callback = 'liqpay_payment_payment_method_configuration_form_elements';
  function __construct()
  {
	$this->currencies = array('UAH', 'RUB', 'USD', 'EUR');
	$this->title = t('Liqpay');
  }
  
  /**
   * Implements PaymentMethodController::validate().
   */
  function validate(Payment $payment, PaymentMethod $payment_method, $strict) {
  }
  
  /**
   * Implements PaymentMethodController::execute().
   */
  function execute(Payment $payment) {
	entity_save('payment', $payment);
	$_SESSION['liqpay_payment_pid'] = $payment->pid;
	drupal_goto(url('liqpay_payment/redirect/'.$payment->pid));
  }
  
  function get_checkout_url()
  {
	return url('https://www.liqpay.com/api/checkout', array('external' => TRUE));
  }
  
  function encode_payment_data($data)
  {
	return base64_encode(json_encode($data));
  }
  function decode_payment_data($encoded_data)
  {
	return json_decode(base64_decode($encoded_data), TRUE);
  }
  
  function get_signature($encoded_data, $settings)
  {
	$private_key = $settings['private_key'];
	return base64_encode( sha1( $private_key . $encoded_data . $private_key, TRUE));
  }
  
  function is_authenticated($post, $settings)
  {
	return $post['signature'] == $this->get_signature($post['data'], $settings);
  }
  
  static function convert_status($status)
  {
	static $status_map = array(
	  'success' => PAYMENT_STATUS_SUCCESS,
	  'failure' => PAYMENT_STATUS_FAILED,
	);
	return @$status_map[$status]?:$status;
  }
  static function save(array $payment_data)
  {
        $table_schema = drupal_get_schema('liqpay_payment');
	$payment_data['pid'] = $payment_data['order_id'];
	$fields_data = Arr::extract($payment_data, array_keys($table_schema['fields']));
	$merge_status = db_merge('liqpay_payment')
	->key(array(
	  'transaction_id' => $payment_data['transaction_id'],
	))
	->fields($fields_data)
	->execute();
  }
  static function process(array $payment_data, $payment) {
    $payment->setStatus(new PaymentStatusItem(self::convert_status($payment_data['status'])));
    entity_save('payment', $payment);
  }
}
